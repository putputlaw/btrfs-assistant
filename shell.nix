{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
    packages = with pkgs;
    [
        cmake
        pkg-config
        libsForQt5.qt5.qttools
        btrfs-progs
        libsForQt5.qt5.qtwayland
        util-linux # findmnt
    ];

    inputsFrom = with pkgs;
    [
        libsForQt5.qt5.qtbase
        libsForQt5.qt5.qttools
        libsForQt5.qt5.qtsvg
    ];

    shellHook = ''
    mkdir -p build
    cd build
    cmake ../CMakeLists.txt
    make
    '';
}

